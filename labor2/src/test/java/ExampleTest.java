import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pom.ExampleSignInPage;

public class ExampleTest {
    private static WebDriver webdriver;
    private ExampleSignInPage exampleSignInPage;

    @BeforeAll
    public static void setupTest() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        webdriver = new ChromeDriver();
        /* Firefox Driver
        System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
		webdriver = new FirefoxDriver(capabilities);
        */
        /* Opera Driver
        System.setProperty("webdriver.opera.driver", "operadriver.exe");
		OperaOptions options = new OperaOptions();
		options.setBinary(
		new File("opera.exe"));
		webdriver = new OperaDriver(options);
        */
    }

    @BeforeEach
    public void navigateToURL() {
        webdriver.navigate().to("URL");
    }

    @Test
    public void sgnInTest() {
        WebElement usernameField = webdriver.findElement(By.id("uernameId"));
        usernameField.sendKeys("username");

        WebElement passwordField = webdriver.findElement(By.xpath("//input[contains(@id, 'passwordId')]"));
        passwordField.sendKeys("password");

        WebDriverWait wait = new WebDriverWait(webdriver, 10);
        WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.className("button")));
        loginButton.click();

        //assert methods can be used to verify if everything went right
    }

    @Test
    public void signInTestWithPOM() {
        exampleSignInPage = new ExampleSignInPage(webdriver);

        exampleSignInPage.writeTextToUsernameField("username");
        exampleSignInPage.writeTextToPasswordField("password");
        exampleSignInPage.pressSignInButton();
    }

    @AfterAll
    public static void quitDriver() {
        webdriver.quit();
    }
}
