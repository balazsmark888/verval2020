# BDD Testing with Cucumber


## Laborfeladat
> **Laborhoz szükséges:** Java, Gradle, IDE *(pl: Eclipse, IntelliJ, egyéb)*, Webdriver *(pl: chromedriver, geckodriver)*
>  
> **Cucumber laborházi:**
> Az előző házi E2E tesztjeihez írjuk meg a megfelelő [Cucumber](https://cucumber.io/) feature fájlokat a megfelelő Scenario-kal és lépésekkel, valamint bontsuk le a már megírt E2E teszteket megfelelő lépésdefiniciókra.
> 
> **HATÁRIDŐ:** csoportodnak megfelelő **utolsó labor**


#### Konfiguráció: 
Az előző laborházihoz tartozó forrás projekt leíró állományát egészítsük ki egy új függőséggel, valamint egy új task-definicióval. 

```groovy
plugins {
    id "se.thinkcode.cucumber-runner" version "0.0.8"
}

dependencies {
    testImplementation 'io.cucumber:cucumber-java:5.6.0'
}

cucumber {
        threads = '4'
        glue = 'stepdefs'
        plugin = ['pretty']
        featurePath = 'src/test/resources'
        main = 'cucumber.api.cli.Main'
    }
```
> Megj. - a `glu`-nak átadott paraméter: a package-nek a neve, ahol a step definition-jeitek találhatóak
> - featurePath- nek átadott paraméter: az elérési útvonal ahhoz a directoryhoz amelyben a .feature állományaitok vannak;

Ha a fentebb látható konfiguráció hozzáadásával nem működik akkor a következő linken találhattok konfigurációkat [lásd a verziókhoz tartozó konfigurációt](https://cucumber.io/docs/tools/java/).

---

## What is cucumber? 
Cucumber is a behavior-driven development (BDD) framework, which is used to write automated acceptance tests. It offers a way to write tests that anybody can understand, regardless of their technical knowledge. 

## Gherkin

  Gherkin is the language that Cucumber uses to define test cases. It is designed to be non-technical, human-readable, and to provide simple documentation of the code under test. 
  The syntax is similar to Python. The structure of a file is defined using whitespace and other control characters. # is used as the line-comment character. Instructions are any non-empty and non-comment lines. They consist of a recognized Gherkin keyword followed by a string.

## Feature files 

  Cucumber tests are divided into individual Features, these are subdivided into Scenarios, which are sequences of Steps. All the feature files end with .feature extension.

## Scenarios 

  Basically, a scenario represents a particular functionality, which is under test. Each scenario should follow **GIVEN**, **WHEN**, and **THEN** format.

## Steps  

  Scenarios are defined by sequences of Steps outlining the preconditions and flow of events that will take place. The first word of a step is a keyword, typically one of:

**Given** - Describes the preconditions and initial state before the start of a test and allows for any pre-test setup that may occur  
**When** - Describes actions taken by a user during a test  
**Then** - Describes the outcome resulting from actions taken in the When clause  

Occasionally, the combination of Given-When-Then uses other keywords to define conjunctions

**And** - Logical and  
**But** - Logically the same as And, but used in the negative form  

Also when every scenario in a feature file has to perform the same steps at the beginning then 

**Background**: Whenever any step is required to perform in each scenario then those steps need to be placed in Background.  
  

```gherkin
Feature: User wants to log in  

Scenario: As a Registered user I want to   login  
   Given I am on the login page   
   When I enter the username as "TOM"  
   And I enter the password as "JERRY"   
   Then I login  
```

## Step Definitions  

  The steps in a feature file can be considered as method invocations. Before Cucumber can execute a step it must be told, via a step definition, how that step should be performed.

  These definitions in these examples are written in Java and can be found in classes that contain those given, when and then statements. Every method has an annotation that corresponds to the step, which is defined by the proper annotation (@Given, @When, @And, @But, @Then). Each phrase starts with “^” so that cucumber understands the start of the step. Similarly, each step ends with “$”. Also, regular expressions can be used to pass different test data. Regular expressions take data from feature steps and pass to step definitions. The order of parameters depends on how they are passed from the feature file. 

```java
public class CucumberTest {
   // regexp - without passing a variable
	@Given("^I am on the login page$")
	public void getLoginPage() {
		System.out.println("Login page");
	}

	// regexp - with passing variable
	@When("^I enter the username as \"(.*)\"$")
	public void enterUsername(String username) {
		System.out.println("Username: " + username);
	}

	//  template - with passing variable
	@And("I enter the password as {string}")
	public void enterPassword(String password) {
		System.out.println("Password: " + password);
	}

	// template - without passing variable
	@Then("I login")
	public void logIn() {
		System.out.println("Logged in");
	}
}
 
```

## Scenario Outline  
  
The Scenario Outline keyword can be used to run the same Scenario multiple times, with different combinations of values.
```gherkin
Feature: data-driven test scenarios

@SmokeTest
Scenario Outline: Data driving new user sings-up
    Given the user is on the landing page
    When the user chooses to sign up
    And user provides the username as "<username>"
    And user provides the password as "<password>"
    And user signs-up
    Then the user should be signed up into the application
    
    Examples:
    | username |  password |
    | user1    |  pass1    |
    | user2    |  pass2    |
    | user3    |  pass3    |

```
The steps can use <> delimited parameters that reference headers in the examples table. Cucumber will replace these parameters with values from the Examples table before it tries to match the step against a step definition.  
  
## Tags  
  
Above Scenarios, useful tags can be placed. Later, in the runner file, we can decide which specific tag (and so as the scenario(s)) we want Cucumber to execute. Tag starts with “@”. After “@” you can have any relevant text to define your tag, like in the example above: @SmokeTest. 
  
## Running from the console    

To execute features/scenarios with a specific tag name:  
```git
gradlew cucumber --tags @SmokeTest  
```  

To run all:  
```git
gradlew cucumber
```

## Configuration
For cucumber to know where the steps are implemented the glue option needs to have the package name where those classes can be found. Also, the feature files location can be given whit the featurePath option.
```git
cucumber {
    glue = 'com.packagename'
    featurePath = 'src/test/resources'
}
```

Also
## Options
The options available can be listed with the command
```git
./gradlew help --task cucumber
```
These options can be used in the cucumber tag in the Gradle build script or in the word options can be given to Cucumber with the syntax like running features/scenarios with a specific tag name.
If the options are defined in the Gradle build script when running the cucumber tests those options will influence the execution.